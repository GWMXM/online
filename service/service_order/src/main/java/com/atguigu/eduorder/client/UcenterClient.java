package com.atguigu.eduorder.client;

import com.atguigu.commonutils.UcenterMemberOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * @author abner
 */
@Component
@FeignClient("service-ucenter")
public interface UcenterClient {
    /**
     * 根据用户id获取用户信息
     * @param id
     * @return
     */
    @PostMapping("/educenter/member/getUserInfoOrder/{id}")
    UcenterMemberOrder getUserInfoOrder(@PathVariable("id") String id);
}
