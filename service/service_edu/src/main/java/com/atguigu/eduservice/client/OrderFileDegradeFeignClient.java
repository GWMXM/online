package com.atguigu.eduservice.client;

import org.springframework.stereotype.Component;

/**
 * @author abner
 */
@Component
public class OrderFileDegradeFeignClient implements OrdersClient {
    @Override
    public Boolean isBuyCourse(String courseId, String memberId) {
        return false;
    }
}
