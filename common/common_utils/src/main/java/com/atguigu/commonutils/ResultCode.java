package com.atguigu.commonutils;

public interface ResultCode {

    /**
     * 成功标识码
     */
    public static Integer SUCCESS = 20000;

    /**
     * 失败标识码
     */
    public static Integer ERROR = 20001;
}
